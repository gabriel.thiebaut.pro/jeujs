var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 300 }
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var bird;
var cursors;

var game = new Phaser.Game(config);

function preload() {
    this.load.image('sky', 'assets/sky.png');
    this.load.image('dude', 'assets/littlegab.png')

}

function create() {
    this.add.image(400, 300, 'sky');

    bird = this.physics.add.image(100, 300, 'dude');

    bird.body.setGravityY(10)
    bird.setBounce(0);
    bird.setCollideWorldBounds(true);

}


function update() {
    cursors = this.input.keyboard.createCursorKeys();
    if (cursors.up.isDown) {
        bird.setVelocityY(-200);
    }
}
